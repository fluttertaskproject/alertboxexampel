import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: PopupMessage(),
  ));
}

class PopupMessage extends StatefulWidget {
  @override
  _PopupMessageState createState() => _PopupMessageState();
}

class _PopupMessageState extends State<PopupMessage> {
  bool _isChecked = false;
  void showPopup1() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Text(
                    'Welcome',
                    style: TextStyle(
                      color: Colors.indigo,
                    ),
                  ),
                ),
                Divider(
                  thickness: 1,
                ),
              ],
            ),
            content: Container(
              child: Text(
                  'Welcome to making your first investment! Proceed to "My Balance" to initiate your first deposit(min. S\$ 1,000")and begin your journey to wealth creation!'),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 70, left: 70),
                child: Container(
                  width: 120,
                  child: RaisedButton(
                      color: Colors.indigo,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        'OK',
                        style: TextStyle(color: Colors.white),
                      )),
                ),
              ),
            ],
          );
        });
  }

  void showPopup2() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Text(
                    'Deposit Minimum',
                    style: TextStyle(
                      color: Colors.indigo,
                    ),
                  ),
                ),
                Divider(
                  thickness: 1,
                ),
              ],
            ),
            content: Container(
              child: Text(
                  'Please deposit a minimum amount of S\$1000 for your first deposit.'),
            ),
            actions: [
              Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 70, left: 70),
                    child: Container(
                      width: 120,
                      child: RaisedButton(
                          color: Colors.indigo,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            'OK',
                            style: TextStyle(color: Colors.white),
                          )),
                    ),
                  ),
                ],
              ),
            ],
          );
        });
  }

  void showPopup3() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Text(
                    '3 steps to making your deposit on BigFundr',
                    style: TextStyle(
                      color: Colors.indigo,
                    ),
                  ),
                ),
                Divider(
                  thickness: 1,
                ),
              ],
            ),
            content: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                    child: Text(
                        '1. You may transfer your funds via internet banking or telegraphic transfer only.')),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: Text(
                      '2. Please retain a copy of your proof of transfer, or take a picture,cleray showing your account name, account number and the amount transferred.'),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: Text(
                      '3. Next, upload your proof of transfer to BigFundr.'),
                ),
                SizedBox(
                  height: 10,
                ),
                CheckboxListTile(
                  title: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text("I do not wish to see this message again."),
                    ],
                  ),
                  value: _isChecked,
                  onChanged: (val) {
                    setState(() {
                      _isChecked = val;
                    });
                  },
                  controlAffinity: ListTileControlAffinity.leading,
                ),
              ],
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 70, left: 70),
                child: Container(
                  width: 120,
                  child: RaisedButton(
                      color: Colors.indigo,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        'continue'.toUpperCase(),
                        style: TextStyle(color: Colors.white),
                      )),
                ),
              ),
            ],
          );
        });
  }

  void showPopup4() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Text(
                    'Insufficient balance',
                    style: TextStyle(
                      color: Colors.indigo,
                    ),
                  ),
                ),
                Divider(
                  thickness: 1,
                ),
              ],
            ),
            content: Container(
              child: Text(
                  'You have insufficient balance to withdraw this amount.'),
            ),
            actions: [
              Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 70, left: 70),
                    child: Container(
                      width: 120,
                      child: RaisedButton(
                          color: Colors.indigo,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            'OK',
                            style: TextStyle(color: Colors.white),
                          )),
                    ),
                  ),
                ],
              ),
            ],
          );
        });
  }

  void showPopup5() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: Text(
                      'Deposit',
                      style: TextStyle(
                        color: Colors.indigo,
                      ),
                    ),
                  ),
                  Divider(
                    thickness: 1,
                  ),
                ],
              ),
              content: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Text(
                          'Transfer from',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      Container(
                        child: Text('Citibank - 3515'),
                      ),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Text(
                          'Transfer method',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      Container(
                        child: Text('Internet Banking'),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Text(
                          'Escrow Agent Bank Details',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Text(
                          'Transfer method',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      Container(
                        child: Text('Internet Banking'),
                      ),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Text(
                          'Bank',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      Container(
                        child: Text('OCBC'),
                      ),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Text(
                          'Name',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      Container(
                        child: Text('BIGFUNDR'),
                      ),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Text(
                          'Account no.',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      Container(
                        child: Text('9999-0000-8888'),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Text(
                          'Deposit Amount (S\$)',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      Container(
                        child: Text(
                          '1,000',
                          style: TextStyle(
                            color: Colors.indigo,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              actions: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 50, right: 50),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      RaisedButton(
                        color: Colors.indigo,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        child: Text('BACK'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      SizedBox(width: 20),
                      RaisedButton(
                        color: Colors.indigo,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        onPressed: () {},
                        child: Text('CONFIRM'),
                      )
                    ],
                  ),
                ),
              ]);
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Popup Message'),
        backgroundColor: Colors.redAccent,
      ),
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                color: Colors.red,
                child: Text('Box1'),
                onPressed: () {
                  showPopup1();
                },
              ),
              Divider(
                height: 1,
                thickness: 1,
              ),
              RaisedButton(
                color: Colors.grey,
                child: Text('Box2'),
                onPressed: () {
                  showPopup2();
                },
              ),
              Divider(
                height: 1,
                thickness: 1,
              ),
              RaisedButton(
                color: Colors.green,
                child: Text('Box3'),
                onPressed: () {
                  showPopup3();
                },
              ),
              Divider(
                height: 1,
                thickness: 1,
              ),
              RaisedButton(
                color: Colors.yellow,
                child: Text('Box4'),
                onPressed: () {
                  showPopup4();
                },
              ),
              Divider(
                height: 1,
                thickness: 1,
              ),
              RaisedButton(
                color: Colors.cyan,
                child: Text('Box5'),
                onPressed: () {
                  showPopup5();
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
